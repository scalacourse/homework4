package adt_and_house

sealed trait Floor

case object RoofingFloor extends Floor
case class LivingFloor(first: Person, second: Person, nextFloor: Floor) extends Floor

case class House(address: String, floor: Floor)

case class Person(age: Int, gender: String)

def protoFold(house: House, acc0: Int)(f: (Int, LivingFloor) => Int): Int = {
	def protoFoldInner(curr: Floor, acc: Int): Int = curr match
		case floor: LivingFloor =>
			protoFoldInner(floor.nextFloor, f(acc, floor))
		case RoofingFloor =>
			acc

	protoFoldInner(house.floor, acc0)
}

def countOldManFloors(house: House, olderThen: Int): Int = protoFold(house, 0) {
	(acc, floor) => {
		def isManOlderThen(person: Person): Boolean = person match
			case Person(age, gender) if age > olderThen && gender == "Man" =>
				true
			case _ =>
				false
		
		acc + (
			if (isManOlderThen(floor.first) || isManOlderThen(floor.second))
				1
			else
				0
		)
	}
}

def womanMaxAge(house: House): Int = protoFold(house, 0) {
	(acc, floor) => {
		def getWomanAge(person: Person): Int = person match
			case Person(age, gender) if gender == "Woman" =>
				age
			case _ =>
				0

		acc max getWomanAge(floor.first) max getWomanAge(floor.second)
	}
}

object Main extends App {
	println("ADT and House")
	
	val houseFloors: Floor =
		LivingFloor(Person(20, "Man"), Person(20, "Man"),
		LivingFloor(Person(10, "Man"), Person(35, "Woman"),
		LivingFloor(Person(10, "Woman"), Person(35, "Woman"),
		RoofingFloor
		)
		)
		)
	
	val house: House = House("Some Random Address", houseFloors)

	println(countOldManFloors(house, 10))
	println(womanMaxAge(house))
}